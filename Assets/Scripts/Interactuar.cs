﻿using UnityEngine;
using System.Collections;

public class Interactuar : MonoBehaviour {

	public bool Mirada;
	private float tiempoinicial;
	private float cuenta;
	public float espera;
	public bool Interactuado = false;

	// Use this for initialization
	void Start () {
		Interactuado = false;
	
	}
	
	// Update is called once per frame
	void Update () {
		
		cuenta = Time.fixedTime;
		Activate();
	
	}
	
	public void Activate(){		
		if(cuenta > tiempoinicial + espera && Mirada == true){
			Interactuado = true;		
		}
	}
	
	public void Gize(){
		Mirada = true;
		tiempoinicial = Time.fixedTime;
	}
	
	public void noGize(){
		Mirada = false;
	}
	
}
