﻿using UnityEngine;
using System.Collections;

public class GenerarEnemy : MonoBehaviour {

	public GameObject[] EnemigosTotales;
	public GameObject[] EnemysCreados;
	public GameObject Player;
	public bool isPlaying;
	public float cuenta;
	public float NextG = 1f;

	// Use this for initialization
	void Start () {
		Player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
		
		//cuenta = Time.fixedTime;
		
		Generation(0,1,1.00f,3.00f,1);
		Generation(1,100,0f,1.00f,1);
		
			
		
	
	}
	
	public void Generation(int puntosminimos, int puntosmaximos,float TimeMinimo,float TimeMaximo, int TypeLimit){
			
		if(Time.fixedTime > NextG){	
			if( puntosminimos <= Player.GetComponent<Points>().Puntos && puntosmaximos > Player.GetComponent<Points>().Puntos ){
				Debug.Log("EnemySalido");
				Instantiate(EnemigosTotales[Random.Range(0,TypeLimit+1)],new Vector3(0,0,0), Quaternion.AngleAxis(Random.Range(0,360), Vector3.up));
				NextG = Random.Range(TimeMinimo,TimeMaximo) + Time.fixedTime;
			} 
		
		}
		
		
	}
}
	