﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	// Use this for initialization
	private bool Mirada;
	private bool interactuado;
	private bool Ataque;
	private GameObject Player;
	private GameObject[] EnemysCreados;
	
	void Awake() {
		EnemysCreados = GameObject.Find("Generator").GetComponent<GenerarEnemy>().EnemysCreados;
	}


	// Use this for initialization
	void Start () {
		Player = GameObject.Find("Player");
		
	}
	
	// Update is called once per frame
	void Update () {	
		
		interactuado = this.gameObject.GetComponent<Interactuar>().Interactuado;
		Mirada = this.gameObject.GetComponent<Interactuar>().Mirada;
		Ataque = this.gameObject.GetComponent<Atacar>().Ataque;
		
			if(interactuado){	
				Destroy(this.gameObject);
				Player.GetComponent<Points>().Puntos++;
				Debug.Log("Auh!");
			}
			
			if(Mirada == true){
				Debug.Log("PILLADO");	
				this.gameObject.GetComponent<BezierMov>().enabled = false;
			} else {		this.gameObject.GetComponent<BezierMov>().enabled = true; }
			
			if(Ataque == true){
				//Player Lives -1
				Destroy(this.gameObject);
			}
			
		LookAtPlayer();
	}
	
	
	void LookAtPlayer(){
		transform.LookAt(Player.transform.position);
	}
}
