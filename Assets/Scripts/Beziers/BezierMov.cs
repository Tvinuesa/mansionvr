﻿using UnityEngine;
using System.Collections;

public class BezierMov : MonoBehaviour {
    public BezierCurve spline;

    public float duration;

    private float progress;

    private void Update()
    {
        progress += Time.deltaTime / duration;
        if (progress > 1f)
        {
            progress = 1f;
        }
        transform.localPosition = spline.GetPoint(progress);
    }
}
